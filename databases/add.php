<?php
    // var_dump($_GET);
    // var_dump($_POST);

    require_once 'config.php';

    $result = false;

    if(!empty($_POST)){
        $name = $_POST['name'];
        $email= $_POST['email'];
        
        $password= md5($_POST['password']);

        // validate

        $sql = "INSERT INTO users(name,email,password) VALUES (:name,:email, :password)";

        $query = $pdo->prepare($sql);

        $result = $query->execute([
            'name'=> $name,
            'email' => $email,
            'password' => $password
        ]);
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Databases</title>
</head>
<body>
    <div class="container">
        <h1>Add User</h1>
        <a href="index.php">Home</a>
        <?php
            if($result == true){
                echo '<div class="alert alert-success">Success!!</div>';
            }
        ?>  

        <form action="add.php" method="post">
            <label for="name">Name</label>
            <input type="text" name="name" id="name">
            <br>
            <label for="email">Email</label>
            <input type="text" name="email" id="email">
            <br>
            <label for="password">Password</label>
            <input type="password" name="password" id="password">
            <br>
            <input type="submit" value="Save">
        </form>
    </div>
</body>
</html>