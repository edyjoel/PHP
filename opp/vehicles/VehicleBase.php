<?php

namespace Vehicles;

abstract class VehicleBase {
        protected $owner;

        public function __construct($owner){
            $this -> owner = $owner;
            echo 'Construct' . '<br>';
        }

        public function __destruct(){
            echo 'Destruct<br>';
        }

        public function move(){
            echo $this -> startEngine();
            echo "moving<br>";
        }

        public function getOwner(){
            return $this->owner;
        }

        public function setOwne($owner){
            $this -> owner = $owner;
        }

        public abstract function startEngine();
    }
