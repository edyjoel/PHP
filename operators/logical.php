<?php

    $v1 = 1;
    $v2 = 1;

    $v3 = 2;
    $v4 = 4;

    $result1 = $v1 == $v2;
    echo 'result1: <br>';
    var_dump($result1);

    $result2 = $v3 == $v4;
    echo 'result2: <br>';
    var_dump($result2);

    // && AND
    // || OR   

    $result3 = $result1 && $result2;
    var_dump($result3);

    $result4 = $result1 || $result2;
    var_dump($result4);
?>