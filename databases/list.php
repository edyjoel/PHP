<?php
    require_once 'config.php';

    $queryResult = $pdo->query("SELECT * FROM users");

    


?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Databases</title>
</head>
<body>
    <div class="container">
        <h1>List Users</h1>
        <a href="index.php">Home</a>
        
        
        <table class="table">
            <tr>
                <th>Name</th>
                <th>Email</th>
            </tr>

            <?php
                while($row = $queryResult->fetch(PDO::FETCH_ASSOC)) {
                    echo '<tr>';
                    echo '<td>' . $row['id'] . '<td>';
                    echo '<td>' . $row['name'] . '<td>';
                    echo '<td>' . $row['email'] . '<td>';

                    echo '</tr>';
                }
            ?>
        </table>
    </div>
</body>
</html>