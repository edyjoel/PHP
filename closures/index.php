<?php

    // $var2 = 1;

    // $var = function() use ($var2) {
    //     echo 'hola';
    //     echo 'Value: ' . $var2;
    // };

    // $var();  

    $x = 3;

    $closure = function($n) use ($x){
        return $n *$x;
    };
    
    $numbers = [1,2,3,4,5];

    $result = array_map( $closure, $numbers);

    var_dump ($result);

?>